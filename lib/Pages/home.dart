import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:direct_web_test/Pages/chat.dart';
import 'package:direct_web_test/Pages/login.dart';
import 'package:direct_web_test/Pages/new.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => HomeState();
}

class HomeState extends State<Home> {
  FirebaseUser myUser;
  DocumentSnapshot myUserDocument;
  bool isLoading = false;

  Future get getMyUser async {
    setState(() => isLoading = !isLoading);

    myUser = await FirebaseAuth.instance.currentUser();

    myUserDocument =
        await Firestore.instance.collection("Users").document(myUser.uid).get();

    print(myUser.uid);

    setState(() => isLoading = !isLoading);
  }

  @override
  void initState() {
    super.initState();
    getMyUser.catchError((error) {
      setState(() => isLoading = !isLoading);
      print(error);
    });
  }

  Future get signOut async {
    await FirebaseAuth.instance.signOut();

    Navigator.of(context).pushAndRemoveUntil(
      CupertinoPageRoute(
        fullscreenDialog: true,
        builder: (context) => Login(),
      ),
      (Route<dynamic> route) => false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text("Chats"),
        trailing: (isLoading)
            ? CupertinoActivityIndicator()
            : FlatButton(
                onPressed: () async {
                  await showDialog(
                    context: context,
                    builder: (context) => AddDialog(myUser, myUserDocument),
                  );
                },
                child: Text("New"),
              ),
        leading: FlatButton(
          onPressed: () async {
            await signOut.catchError((error) => print(error));
          },
          child: Text("Sign out"),
        ),
      ),
      child: (isLoading)
          ? Center(
              child: CupertinoActivityIndicator(),
            )
          : StreamBuilder(
              stream: Firestore.instance
                  .collection("Users")
                  .document(myUser.uid)
                  .collection("Chats")
                  .orderBy("Date", descending: true)
                  .snapshots(),
              builder: (context, querySnapshot) {
                if (!querySnapshot.hasData)
                  return Center(
                    child: CupertinoActivityIndicator(),
                  );

                List<DocumentSnapshot> chats = querySnapshot.data?.documents;

                if (chats.isEmpty)
                  return Center(
                    child: Material(
                      child: Text("No Chats"),
                    ),
                  );

                return ListView.builder(
                  itemCount: chats.length,
                  itemBuilder: (context, index) {
                    DocumentSnapshot chat = chats[index];

                    return Material(
                      child: ListTile(
                        onTap: () {
                          Navigator.of(context).push(CupertinoPageRoute(
                              builder: (context) =>
                                  Chat(chat, myUserDocument)));
                        },
                        title: Text((chat["Person 1 Username"] ==
                                myUserDocument["Username"])
                            ? chat["Person 2 Username"]
                            : chat["Person 1 Username"]),
                      ),
                    );
                  },
                );
              },
            ),
    );
  }
}
