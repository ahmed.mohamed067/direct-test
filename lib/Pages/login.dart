import 'package:direct_web_test/Pages/home.dart';
import 'package:direct_web_test/Pages/register.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  FocusNode passwordFocusNode = new FocusNode();

  String email, password;
  bool isLoading = false;

  Future get checkUser async {
    var user = await FirebaseAuth.instance.currentUser();
    if (user != null) {
      print(user.uid);
      Navigator.of(context).pushAndRemoveUntil(
        CupertinoPageRoute(
          fullscreenDialog: true,
          builder: (context) => Home(),
        ),
        (Route<dynamic> route) => false,
      );
    }
  }

  @override
  void initState() {
    super.initState();
    checkUser.catchError((error) => print(error));
  }

  Future get loginWithEmailAndPassword async {
    if (formKey.currentState.validate()) {
      setState(() => isLoading = !isLoading);

      await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: email,
        password: password,
      );

      setState(() => isLoading = !isLoading);

      Navigator.of(context).pushAndRemoveUntil(
        CupertinoPageRoute(
          fullscreenDialog: true,
          builder: (context) => Home(),
        ),
        (Route<dynamic> route) => false,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    var emailWidget = Padding(
      padding: EdgeInsets.only(bottom: 10),
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        textInputAction: TextInputAction.next,
        controller: emailController,
        onFieldSubmitted: (value) {
          FocusScope.of(context).requestFocus(passwordFocusNode);
        },
        validator: (text) {
          if (text.isEmpty) {
            return "Cannot leave empty";
          } else {
            setState(() => email = text);
          }
        },
        decoration: InputDecoration(
          hintText: "Email Address",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
          ),
        ),
      ),
    );

    var passwordWidget = Padding(
      padding: EdgeInsets.only(bottom: 10),
      child: TextFormField(
        keyboardType: TextInputType.text,
        obscureText: true,
        controller: passwordController,
        focusNode: passwordFocusNode,
        textInputAction: TextInputAction.done,
        onFieldSubmitted: (value) {
          passwordFocusNode.unfocus();
        },
        validator: (text) {
          if (text.isEmpty) {
            return "Cannot leave empty";
          } else {
            setState(() => password = text);
          }
        },
        decoration: InputDecoration(
          hintText: "Password",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
          ),
        ),
      ),
    );

    var login = (isLoading)
        ? CupertinoActivityIndicator()
        : CupertinoButton(
            onPressed: () async {
              await loginWithEmailAndPassword.catchError(
                (error) {
                  print(error);
                  setState(() => isLoading = !isLoading);
                },
              );
            },
            color: Colors.lightBlue[900],
            child: Text("Login"),
          );

    var register = (isLoading)
        ? SizedBox()
        : FlatButton(
            child: Text("Register"),
            onPressed: () {
              Navigator.of(context).push(
                CupertinoPageRoute(
                  fullscreenDialog: true,
                  builder: (context) => Register(),
                ),
              );
            },
          );

    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text("Login"),
      ),
      child: Material(
        child: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  emailWidget,
                  passwordWidget,
                  login,
                  register,
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
