import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Chat extends StatefulWidget {
  Chat(this.chat, this.myDocument);

  final DocumentSnapshot chat, myDocument;

  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  Map<String, dynamic> message = new Map();
  TextEditingController controller = TextEditingController();
  bool isLoading = false;

  Future get sendMessage async {
    setState(() => isLoading = !isLoading);

    if (formKey.currentState.validate()) {
      controller.clear();
      
      await Firestore.instance
          .collection("Chats")
          .document(widget.chat.documentID)
          .collection("Messages")
          .add(message);

      message = new Map();
    }

    setState(() => isLoading = !isLoading);
  }

  @override
  Widget build(BuildContext context) {
    var textField = Expanded(
      child: Form(
        key: formKey,
        child: TextFormField(
          keyboardType: TextInputType.text,
          textInputAction: TextInputAction.send,
          controller: controller,
          validator: (text) {
            if (text.isEmpty) {
              return "Type Something";
            } else {
              message = {
                "From": widget.myDocument["Username"],
                "To": (widget.chat["Person 1 Username"] ==
                        widget.myDocument["Username"])
                    ? widget.chat["Person 2 Username"]
                    : widget.chat["Person 1 Username"],
                "Message": text,
                "Date": DateTime.now(),
              };
            }
          },
          decoration: InputDecoration(
            hintText: "Type,..",
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15),
            ),
          ),
        ),
      ),
    );

    var sendButton = (isLoading)
        ? CupertinoActivityIndicator()
        : IconButton(
            icon: Icon(Icons.send),
            onPressed: () async {
              await sendMessage.catchError((error) {
                print(error);
                setState(() => isLoading = !isLoading);
              });
            },
          );

    var type = Material(
      color: Colors.black12,
      child: SafeArea(
        top: false,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          child: Row(
            children: <Widget>[
              textField,
              sendButton,
            ],
          ),
        ),
      ),
    );

    var messages = Expanded(
      child: StreamBuilder(
        stream: Firestore.instance
            .collection("Chats")
            .document(widget.chat.documentID)
            .collection("Messages")
            .orderBy("Date", descending: true)
            .snapshots(),
        builder: (context, querySnapshot) {
          if (!querySnapshot.hasData)
            return Center(
              child: CupertinoActivityIndicator(),
            );

          List<DocumentSnapshot> messages = querySnapshot.data?.documents;

          if (messages.isEmpty)
            return Center(
              child: Material(
                child: Text("No Messages"),
              ),
            );

          return ListView.builder(
            reverse: true,
            itemCount: messages.length,
            itemBuilder: (context, index) {
              DocumentSnapshot message = messages[index];

              return Padding(
                padding: EdgeInsets.only(
                  top: 10,
                  right: (message["From"] == widget.myDocument["Username"])
                      ? 10
                      : MediaQuery.of(context).size.width / 7,
                  left: (message["From"] != widget.myDocument["Username"])
                      ? 10
                      : MediaQuery.of(context).size.width / 7,
                ),
                child: Material(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15),
                    bottomLeft: Radius.circular(
                      (message["From"] != widget.myDocument["Username"])
                          ? 0
                          : 15,
                    ),
                    bottomRight: Radius.circular(
                      (message["From"] == widget.myDocument["Username"])
                          ? 0
                          : 15,
                    ),
                  ),
                  color: (message["From"] == widget.myDocument["Username"])
                      ? Colors.blue
                      : Colors.black38,
                  child: Padding(
                    padding: EdgeInsets.all(15),
                    child: Text(message["Message"]),
                  ),
                ),
              );
            },
          );
        },
      ),
    );

    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text(
          (widget.chat["Person 1 Username"] == widget.myDocument["Username"])
              ? widget.chat["Person 2 Username"]
              : widget.chat["Person 1 Username"],
        ),
      ),
      child: Column(
        children: <Widget>[
          messages,
          type,
        ],
      ),
    );
  }
}
