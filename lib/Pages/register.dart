import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:direct_web_test/Pages/home.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  FocusNode emailFocusNode = new FocusNode();
  FocusNode passwordFocusNode = new FocusNode();
  FocusNode password2FocusNode = new FocusNode();

  String username, email, password;

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  Future get registerWithEmailAndPassword async {
    if (formKey.currentState.validate()) {
      setState(() => isLoading = !isLoading);
      var authResult =
          await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );

      await Firestore.instance
          .collection("Users")
          .document(authResult.user.uid)
          .setData({
        "Username": username,
        "Email Address": email,
        "UID": authResult.user.uid,
      });

      setState(() => isLoading = !isLoading);

      Navigator.of(context).pushAndRemoveUntil(
        CupertinoPageRoute(
          fullscreenDialog: true,
          builder: (context) => Home(),
        ),
        (Route<dynamic> route) => false,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    var usernameWidget = Padding(
      padding: EdgeInsets.only(bottom: 10),
      child: TextFormField(
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        onFieldSubmitted: (value) {
          FocusScope.of(context).requestFocus(emailFocusNode);
        },
        validator: (text) {
          if (text.isEmpty) {
            return "Cannot leave empty";
          } else {
            setState(() => username = text);
          }
        },
        decoration: InputDecoration(
          hintText: "Username",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
          ),
        ),
      ),
    );

    var emailWidget = Padding(
      padding: EdgeInsets.only(bottom: 10),
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        textInputAction: TextInputAction.next,
        focusNode: emailFocusNode,
        onFieldSubmitted: (value) {
          FocusScope.of(context).requestFocus(passwordFocusNode);
        },
        validator: (text) {
          if (text.isEmpty) {
            return "Cannot leave empty";
          } else {
            setState(() => email = text);
          }
        },
        decoration: InputDecoration(
          hintText: "Email Address",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
          ),
        ),
      ),
    );

    var passwordWidget = Padding(
      padding: EdgeInsets.only(bottom: 10),
      child: TextFormField(
        keyboardType: TextInputType.text,
        obscureText: true,
        focusNode: passwordFocusNode,
        textInputAction: TextInputAction.next,
        onFieldSubmitted: (value) {
          FocusScope.of(context).requestFocus(password2FocusNode);
        },
        validator: (text) {
          if (text.isEmpty) {
            return "Cannot leave empty";
          } else {
            setState(() => password = text);
          }
        },
        decoration: InputDecoration(
          hintText: "Password",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
          ),
        ),
      ),
    );

    var password2Widget = Padding(
      padding: EdgeInsets.only(bottom: 10),
      child: TextFormField(
        keyboardType: TextInputType.text,
        obscureText: true,
        focusNode: password2FocusNode,
        textInputAction: TextInputAction.done,
        onFieldSubmitted: (value) {
          password2FocusNode.unfocus();
        },
        validator: (text) {
          if (text.isEmpty) {
            return "Cannot leave empty";
          } else if (password != text) {
            return "Password doesn't match";
          } else {}
        },
        decoration: InputDecoration(
          hintText: "Password",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
          ),
        ),
      ),
    );

    var register = (isLoading)
        ? CupertinoActivityIndicator()
        : CupertinoButton(
            onPressed: () async {
              await registerWithEmailAndPassword.catchError(
                (error) {
                  print(error);
                  setState(() => isLoading = !isLoading);
                },
              );
            },
            color: Colors.lightBlue[900],
            child: Text("Register"),
          );

    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text("Register"),
      ),
      child: Material(
        child: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  usernameWidget,
                  emailWidget,
                  passwordWidget,
                  password2Widget,
                  register,
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
