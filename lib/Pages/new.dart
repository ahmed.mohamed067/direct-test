import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class AddDialog extends StatefulWidget {
  AddDialog(this.myUser, this.myUserDocument);

  final DocumentSnapshot myUserDocument;
  final FirebaseUser myUser;

  @override
  _AddDialogState createState() => _AddDialogState(myUser, myUserDocument);
}

class _AddDialogState extends State<AddDialog> {
  _AddDialogState(this.myUser, this.myUserDocument);

  final DocumentSnapshot myUserDocument;
  final FirebaseUser myUser;

  TextEditingController controller = new TextEditingController();
  bool isLoading = false;

  Future get addUserEmail async {
    if (controller.text.isNotEmpty) {
      DocumentSnapshot user;
      setState(() => isLoading = !isLoading);

      await Firestore.instance
          .collection("Users")
          .where("Email Address", isEqualTo: controller.text)
          .getDocuments()
          .then(
        (query) {
          print(query.documents.length);

          user = query.documents[0];
        },
      );

      if (user != null) {
        DocumentReference chat =
            await Firestore.instance.collection("Chats").add({
          "Person 1 Username": myUserDocument["Username"],
          "Person 2 Username": user["Username"],
          "Date": DateTime.now(),
        });

        await Firestore.instance
            .collection("Users")
            .document(myUser.uid)
            .collection("Chats")
            .document(chat.documentID)
            .setData({
          "Chat ID": chat.documentID,
          "Person 1 Username": myUserDocument["Username"],
          "Person 2 Username": user["Username"],
          "Date": DateTime.now(),
        });

        await Firestore.instance
            .collection("Users")
            .document(user["UID"])
            .collection("Chats")
            .document(chat.documentID)
            .setData({
          "Chat ID": chat.documentID,
          "Person 1 Username": myUserDocument["Username"],
          "Person 2 Username": user["Username"],
          "Date": DateTime.now(),
        });

        Navigator.of(context).pop();
      }
      setState(() => isLoading = !isLoading);
    }
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: Text("Add Email"),
      content: Material(
        color: Colors.transparent,
        child: TextFormField(
          controller: controller,
          decoration: InputDecoration(
            hintText: "User Email Address",
          ),
        ),
      ),
      actions: <Widget>[
        CupertinoButton(
          child: Text(
            "Cancel",
            style: TextStyle(
              color: Colors.red,
            ),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        (isLoading)
            ? CupertinoActivityIndicator()
            : CupertinoButton(
                child: Text(
                  "Add",
                  style: TextStyle(
                    color: Colors.blue,
                  ),
                ),
                onPressed: () async {
                  await addUserEmail.catchError((error) {
                    print(error);
                    setState(() => isLoading = !isLoading);
                  });
                },
              )
      ],
    );
  }
}
